// Copyright 2017 The goimagehash Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package goimagehash

import (
	"errors"
)

// HashKind describes the kinds of Hash.
type HashKind int

// ImageHash is a struct of Hash computation.
type ImageHash struct {
	Hash uint64
	Kind HashKind
}

const (
	// Unknown is a enum value of the unknown Hash.
	Unknown HashKind = iota
	// AHash is a enum value of the average Hash.
	AHash
	//PHash is a enum value of the perceptual Hash.
	PHash
	// DHash is a enum value of the difference Hash.
	DHash
	// WHash is a enum value of the wavelet Hash.
	// WHash
)

// NewImageHash function creates a new image Hash.
func NewImageHash(hash uint64, kind HashKind) *ImageHash {
	return &ImageHash{Hash: hash, Kind: kind}
}

// Distance method returns a distance between two hashes.
func (h *ImageHash) Distance(other *ImageHash) (int, error) {
	if h.GetKind() != other.GetKind() {
		return -1, errors.New("Image hashes's Kind should be identical.")
	}

	diff := 0
	lhash := h.GetHash()
	rhash := other.GetHash()

	hamming := lhash ^ rhash
	for hamming != 0 {
		diff += int(hamming & 1)
		hamming >>= 1
	}

	return diff, nil
}

// GetHash method returns a 64bits Hash value.
func (h *ImageHash) GetHash() uint64 {
	return h.Hash
}

// GetKind method returns a Kind of image Hash.
func (h *ImageHash) GetKind() HashKind {
	return h.Kind
}

// Set method sets a bit of index.
func (h *ImageHash) Set(idx int) {
	h.Hash |= 1 << uint(idx)
}
